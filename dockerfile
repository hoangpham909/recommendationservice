FROM openjdk:19-slim
COPY target/*.jar /home/app/customerapi.jar
ENTRYPOINT ["java", "-jar", "/home/app/customerapi.jar"]