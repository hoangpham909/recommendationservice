package com.example.recommendation.controller;

import com.example.recommendation.model.Recommendation;
import com.example.recommendation.repository.RecommendationRepository;
import com.example.recommendation.service.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RecommendationController {

    @Autowired
    private RecommendationRepository recommendationRepository;
    @Autowired
    private RecommendationService recommendationService;

    @GetMapping("/recommendations")
    public List<Recommendation> getAllRecommendations(){
        return recommendationRepository.findAll();
    }
    @GetMapping("/recommendations/{productID}")
    public List<Recommendation> getAllRecommendationsByProductID(@PathVariable int productID){
        return recommendationService.findAllByProductID(productID);
    }
    @PostMapping("/recommendation")
    public void addRecommendation(@RequestBody Recommendation recommendation) {
        recommendationRepository.save(recommendation);
    }

    @DeleteMapping("/recommendation/{productID}")
    public void deleteRecommendationsByProductID(@PathVariable int productID){
        recommendationService.deleteRecommendationsByProductID(productID);
    }










}
