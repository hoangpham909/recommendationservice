package com.example.recommendation.repository;

import com.example.recommendation.model.Recommendation;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecommendationRepository extends JpaRepository<Recommendation, Integer> {
    @Query(value = "select * from Recommendation where productID = ?", nativeQuery = true)
    List<Recommendation> findAllByProductID(int productID);

    @Modifying
    @Transactional
    @Query(value = "delete from Recommendation where productID = ?", nativeQuery = true)
    void deleteRecommendationsByProductID(int productID);
}
