package com.example.recommendation.service;

import com.example.recommendation.model.Recommendation;
import com.example.recommendation.repository.RecommendationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecommendationService {
    @Autowired
    RecommendationRepository recommendationRepository;
    public List<Recommendation> findAllByProductID(int productID) {
        return recommendationRepository.findAllByProductID(productID);
    }

    public void deleteRecommendationsByProductID(int productID) {
        recommendationRepository.deleteRecommendationsByProductID(productID);
    }
}
